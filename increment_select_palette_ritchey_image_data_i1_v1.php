<?php
#Name:Increment Select Palette Ritchey Image Data i1 v1
#Description:Increment data conformed to Ritchey Image Data v1 by 1 valid increment using content generation techniques, and a restricted palette. Returns data as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image data must contain a field called "Colour", and it must have an RGB colour code from the select palette as the value. Any other fields will not be included in the new data. Width is NOT preserved. The width, and height will be made as equal as possible. Colours not from the palette will be treated as max colour value.
#Arguments:'string' is a string containing the data to increment. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('increment_select_palette_ritchey_image_data_i1_v1') === FALSE){
function increment_select_palette_ritchey_image_data_i1_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Break into an array of of each pixel's "Colour" field value. Discard all other data, including line endings. Reverse the array so that increment can be done from end to start of image.
		@preg_match_all("/Colour:.*?\./", $string, $select_data, PREG_PATTERN_ORDER);
		$select_data = @implode($select_data[0]);
		$select_data = @str_replace('Colour:', "", $select_data);
		$select_data = @trim($select_data);
		$select_data = @explode('.', $select_data);
		$discard = @array_pop($select_data);
		$select_data = @array_reverse($select_data);
		###Increment the first pixel colour. When it exceeds max value, loop back to start value, and increment the next pixel. If there is no next pixel, create one with a start value.
		foreach ($select_data as &$value){
			if ($value === '0,0,0'){
				$value = '61,61,61';
				goto break_increment;
			} else if ($value === '61,61,61'){
				$value = '132,132,132';
				goto break_increment;
			} else if ($value === '132,132,132'){
				$value = '187,187,187';
				goto break_increment;
			} else if ($value === '187,187,187'){
				$value = '255,255,255';
				goto break_increment;
			} else {
				$value = '0,0,0';
			}
		}
		unset($value);
		break_increment:
		###Unreverse array
		$select_data = @array_reverse($select_data);
		###Check if entire array is '0,0,0' values, and if it is add a new value of '0,0,0' so that a new pixel is added.
		$check = FALSE;
		foreach ($select_data as &$value) {
			if ($value !== '0,0,0'){
				$check = TRUE;
			}
		}
		unset($value);
		if ($check === FALSE){
			$select_data[] = '0,0,0';
		}
		###Apply required data around new colour value
		foreach ($select_data as &$value) {
			$value = "[Colour:{$value}.]";
		}
		unset($value);
		###Determine width, apply it, and convert to string
		$width = @count($select_data);
		$width = @sqrt($width);
		$width = @floor($width);
		$select_data = @array_chunk($select_data, $width);
		foreach ($select_data as &$value) {
			$value = @implode($value);
		}
		unset($value);
		$select_data = @implode("\n", $select_data);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('increment_select_palette_ritchey_image_data_i1_v1_format_error') === FALSE){
				function increment_select_palette_ritchey_image_data_i1_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("increment_select_palette_ritchey_image_data_i1_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $select_data;
	} else {
		return FALSE;
	}
}
}
?>